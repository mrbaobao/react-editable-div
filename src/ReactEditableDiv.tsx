import * as React from "react"
/**
 * Class list
 */
const ClassList = {
    block: "ed-block",
    currentBlock: "ed-current-block",
    imgSmall: "ed-small",
}
/**
 * Data for each item in content
 */
interface EItem {
    //Type
    type: "block" | "text" | "image" | "b" | "i" | "span" | "br"
    //Data
    text?: string
    //For image
    src?: string
    size?: string
    alt?: string
    content?: EItem[]
    isCurrentBlock?: boolean
}
/**
 * Props
 */
interface EProps {
    //Class name
    className?: string
    //Content
    content: EItem[]
    //Onchange
    onChange: any
    //Delete
    onDeleteMe: any
    //Change caret
    onChangeCaret?: any
    //Placeholder
    placeholder?: string


}
/**
 * State
 */
interface EState {
    //Content
    content: EItem[]
    willDeleteMe: boolean
    currentBlockIndex: number
}
/**
 * Generate content
 */
export const generateEContent = (content: EItem[]) => {
    const generateItem = (item: EItem, i: number) => {
        //Image
        if (item.type === "image") {
            return <img className={item.size === "small" ? " ed-small " : ""} src={item.src} key={i} alt={item.alt} />
        }
        //Text
        if (item.type === "text") {
            return item.text
        }
        //Text Bold
        if (item.type === "b") {
            return <b key={i}>{item.text}</b>
        }
        //Text Italic
        if (item.type === "i") {
            return <i key={i}>{item.text}</i>
        }
        //Text Span
        if (item.type === "span") {
            return <span key={i}>{item.text}</span>
        }
        //BR
        if (item.type === "br") {
            return <br key={i} />
        }
        //Null
        return null
    }
    const generateBlock = (block: EItem[]) => {
        if (block.length === 0) {
            return <br />
        }
        return (
            block.map((item: EItem, i: number) => {
                return generateItem(item, i)
            })
        )
    }

    return (
        content.map((item: EItem, i: number) => {
            //Block
            if (item.type === "block" && item.content) {
                return <div className={ClassList.block + " " + (item.isCurrentBlock ? ClassList.currentBlock : "")} key={i}>{generateBlock(item.content)}</div>
            }
            //Inline
            return generateItem(item, i)
        })
    )
}
/**
 * Parse html from div inner html to json[]
 * @param div 
 */
export const parseEditableDivContent = (wrapper: HTMLDivElement) => {
    /**
     * Parse an html element to data
     * @param item 
     */
    const parseNode = (node: Node): EItem | null => {
        //Image:
        let image: HTMLImageElement
        /**
         * Image
         */
        if (node.nodeName === "IMG") {
            image = node as HTMLImageElement
            return {
                type: "image",
                src: image.getAttribute("src") || undefined,
                size: image.classList.contains(ClassList.imgSmall) ? "small" : "normal"
            }
        }
        //TEXT
        if (node.textContent) {
            /**
                     * Text
                     */
            if (node.nodeName === "#text") {
                return {
                    type: "text",
                    text: node.textContent
                }
            }
            /**
             * Bold text
             */
            if (node.nodeName === "B") {
                return {
                    type: "b",
                    text: node.textContent
                }
            }
            /**
             * Italic text
             */
            if (node.nodeName === "I") {
                return {
                    type: "i",
                    text: node.textContent
                }
            }
            /**
             * Span
             */
            if (node.nodeName === "SPAN") {
                return {
                    type: "span",
                    text: node.textContent
                }
            }
            //Not valid:
            return {
                type: "text",
                text: node.textContent
            }
        }
        //BR
        if (node.nodeName === "BR") {
            return {
                type: "br",
            }
        }
        //Null
        return null
    }
    /**
     * Parse a block content
     * @param block 
     */
    const parseBlock = (block: HTMLDivElement) => {
        //Content
        let content: any[] = []
        //Children
        const nodes = block.childNodes
        let itemData: EItem | null
        for (let i = 0; i < nodes.length; i++) {
            itemData = parseNode(nodes[i])
            if (itemData) {
                content.push(itemData)
            }
        }
        //Return
        return content
    }
    //Content
    let content: any[] = []
    //Children
    const nodes = wrapper.childNodes
    let node: Node, itemData: EItem | null, divBlock: HTMLDivElement
    let currentBlockIndex = 0, isCurrentBlock = false
    for (let i = 0; i < nodes.length; i++) {
        node = nodes[i]
        /**
         * Div
         */
        if (node.nodeName === "DIV") {
            divBlock = node as HTMLDivElement
            isCurrentBlock = divBlock.classList.contains(ClassList.currentBlock)
            content.push({
                type: "block",
                content: parseBlock(divBlock),
                isCurrentBlock
            })
            //currentBlockIndex
            if (isCurrentBlock) {
                currentBlockIndex = i
            }
            //NEXT
            continue
        }
        itemData = parseNode(node)
        if (itemData) {
            content.push({
                type: "block",
                content: [itemData]
            })
        }
    }
    //Return
    return { content, currentBlockIndex }
}
/**
 * Validate url of an image
 */
const isImgUrl = (url: string, httpsOnly: boolean = true): boolean => {

    let regEx = /(http(s?):)([/|.|\w|\s|\*|-])*\.(?:jpg|gif|png)/g
    if (httpsOnly) {
        regEx = /(https:)([/|.|\w|\s|\*|-])*\.(?:jpg|gif|png)/g
    }
    if (url.match(regEx) === null) {

        /**
         * Base 64
         */
        const base64Prefix = {
            jpg: "data:image/jpeg;base64,",
            png: "data:image/png;base64,",
        }
        //OK
        return url.startsWith(base64Prefix.png) || url.startsWith(base64Prefix.png)
    }
    //OK
    return true
}
/**
 * Editable div
 */
class EditableDiv extends React.Component<EProps, EState>{
    //Virtual content
    virtualContent: HTMLDivElement | null = null
    //View content
    content: HTMLDivElement | null = null
    height: number = 0
    /**
     * State
     */
    constructor(props: EProps) {
        super(props)
        /**
         * State:
         */
        this.state = {
            content: this.props.content,
            willDeleteMe: false,
            currentBlockIndex: -1
        }
    }
    /**
     * Did mount
     * 1. sync content from virtual to view
     */
    componentDidMount() {
        this.syncContent()
        //Height
        if (this.content?.clientHeight) {
            this.height = this.content.clientHeight
        }

    }
    componentDidUpdate() {
        this.syncContent()
        //Height
        if(this.content?.clientHeight) {
            this.height = this.content?.clientHeight
        }
    }
    /**
     * Sync content
     */
    syncContent = () => {
        //Pass data
        if(this.content && this.virtualContent?.innerHTML) {
            this.content.innerHTML = this.virtualContent?.innerHTML
        }
    }
    /**
     * Update content
     */
    updateContent = () => {
        if(!this.content) {
            return
        }
        const { content, currentBlockIndex } = parseEditableDivContent(this.content)
        this.setState({
            content,
            currentBlockIndex
        })
        //Trigger event
        const { onChange } = this.props
        if (onChange) {
            onChange({ content, currentBlockIndex })
        }
    }
    /**
     * Handle paste
     */
    handlePaste = (e: React.ClipboardEvent) => {
        //Selection
        const selection = document.getSelection()
        if(!selection) {
            return
        }
        //Range
        const range = selection.getRangeAt(0)
        //Check clipboard
        const clipboardData = e.clipboardData
        const text = clipboardData.getData("url") || clipboardData.getData("text")
        const pasteText = () => {
            //Insert image to div
            const div = document.createElement("div")
            div.innerHTML = text
            range.insertNode(div)
            // Update new content
            this.updateContent()
        }
        if (isImgUrl(text)) {
            //Load image ok --> Prevent Paste
            e.preventDefault()
            //Load image
            const image = new Image()
            image.src = text
            //Remove selection
            selection.empty()
            //Load event
            image.onload = () => {
                //Insert image to div
                range.insertNode(image)
                //Update new content
                this.updateContent()
            }
            image.onerror = () => {
                pasteText()
            }
            //Stop
            return
        }
        //Update new content
        setTimeout(() => {
            this.updateContent()
        }, 100)
    }
    /**
     * Blur --> Update
     */
    handleBlurContent = () => {
        if(!this.content) {
            return
        }
        //Check caret
        this.checkCaretChange()
        //Content change
        if (this.previousContent == this.content.innerHTML) {
            return
        }
        this.updateContent()
    }
    /**
     * Caret change
     */
    checkCaretChange = () => {
        //Save range
        const selection = document.getSelection()
        const range: any = selection && !selection.empty && selection.getRangeAt(0)

        let startNode = range && range.startContainer
        while (startNode && startNode.nodeName !== "DIV") {
            startNode = startNode.parentNode
        }
        if(!this.content) {
            return
        }
        const blocks = this.content.getElementsByClassName(ClassList.block)
        for (let i = 0; i < blocks.length; i++) {
            blocks[i].classList.remove(ClassList.currentBlock)
        }
        const currentBlock = startNode as HTMLDivElement
        currentBlock && currentBlock.classList.add(ClassList.currentBlock)

        const { onChangeCaret } = this.props
        if (onChangeCaret) {
            onChangeCaret()
        }
    }
    previousContent: string = ""
    handleFocusContent = () => {
        if(!this.content) {
            return
        }
        this.previousContent = this.content.innerHTML
    }
    /**
     * handleKeyDown
     */
    handleKeyDown = (e: any) => {
        if(!this.content) {
            return
        }
        const { willDeleteMe } = this.state
        const BACKSPACE_KEYCODE = 8
        if (e.keyCode === BACKSPACE_KEYCODE) {

            if (!this.content.innerHTML) {
                if (willDeleteMe) {
                    const { onDeleteMe } = this.props
                    //onDeleteMe
                    if (onDeleteMe) {
                        onDeleteMe()
                    }
                } else {
                    this.setState({
                        willDeleteMe: true,
                        content: []
                    })
                }
                //Stop
                return
            }
        }
        if (willDeleteMe) {
            this.setState({
                willDeleteMe: false
            })
        }

        // if (this.height !== this.content.clientHeight){
        //     this.updateContent()
        // }
    }
    handleDoubleClickContent = (e: any) => {
        const target = e.target as HTMLElement
        if (target && target.nodeName === "IMG") {
            target.classList.toggle(ClassList.imgSmall)
        }
    }
    //Render
    render() {
        const { className, placeholder } = this.props
        const { content, willDeleteMe } = this.state
        const style = {
            background: "white",
            border: willDeleteMe ? "1px solid red" : "initial"
        }
        return (
            <>
                {/* Editor */}
                <div ref={c => this.content = c}
                    style={style}
                    placeholder={placeholder}
                    contentEditable={true}
                    suppressContentEditableWarning={true}
                    onPaste={this.handlePaste}
                    onBlur={this.handleBlurContent}
                    onFocus={this.handleFocusContent}
                    onKeyDown={this.handleKeyDown}
                    onDoubleClick={this.handleDoubleClickContent}
                    className={"ed-content " + (className ?? "")}></div>
                {/** Virtual content*/}
                <div ref={v => this.virtualContent = v}
                    style={{ visibility: "hidden", position: "absolute", left: "-9999999px", top: "-9999999px" }}>
                    {generateEContent(content)}
                </div>
            </>
        )
    }
}
//Export
export default EditableDiv