import * as React from "react";
/**
 * Data for each item in content
 */
interface EItem {
    type: "block" | "text" | "image" | "b" | "i" | "span" | "br";
    text?: string;
    src?: string;
    size?: string;
    alt?: string;
    content?: EItem[];
    isCurrentBlock?: boolean;
}
/**
 * Props
 */
interface EProps {
    className?: string;
    content: EItem[];
    onChange: any;
    onDeleteMe: any;
    onChangeCaret?: any;
    placeholder?: string;
}
/**
 * State
 */
interface EState {
    content: EItem[];
    willDeleteMe: boolean;
    currentBlockIndex: number;
}
/**
 * Generate content
 */
export declare const generateEContent: (content: EItem[]) => (string | JSX.Element | null | undefined)[];
/**
 * Parse html from div inner html to json[]
 * @param div
 */
export declare const parseEditableDivContent: (wrapper: HTMLDivElement) => {
    content: any[];
    currentBlockIndex: number;
};
/**
 * Editable div
 */
declare class EditableDiv extends React.Component<EProps, EState> {
    virtualContent: HTMLDivElement | null;
    content: HTMLDivElement | null;
    height: number;
    /**
     * State
     */
    constructor(props: EProps);
    /**
     * Did mount
     * 1. sync content from virtual to view
     */
    componentDidMount(): void;
    componentDidUpdate(): void;
    /**
     * Sync content
     */
    syncContent: () => void;
    /**
     * Update content
     */
    updateContent: () => void;
    /**
     * Handle paste
     */
    handlePaste: (e: React.ClipboardEvent) => void;
    /**
     * Blur --> Update
     */
    handleBlurContent: () => void;
    /**
     * Caret change
     */
    checkCaretChange: () => void;
    previousContent: string;
    handleFocusContent: () => void;
    /**
     * handleKeyDown
     */
    handleKeyDown: (e: any) => void;
    handleDoubleClickContent: (e: any) => void;
    render(): JSX.Element;
}
export default EditableDiv;
