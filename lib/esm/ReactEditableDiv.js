"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseEditableDivContent = exports.generateEContent = void 0;
const React = __importStar(require("react"));
/**
 * Class list
 */
const ClassList = {
    block: "ed-block",
    currentBlock: "ed-current-block",
    imgSmall: "ed-small",
};
/**
 * Generate content
 */
exports.generateEContent = (content) => {
    const generateItem = (item, i) => {
        //Image
        if (item.type === "image") {
            return React.createElement("img", { className: item.size === "small" ? " ed-small " : "", src: item.src, key: i, alt: item.alt });
        }
        //Text
        if (item.type === "text") {
            return item.text;
        }
        //Text Bold
        if (item.type === "b") {
            return React.createElement("b", { key: i }, item.text);
        }
        //Text Italic
        if (item.type === "i") {
            return React.createElement("i", { key: i }, item.text);
        }
        //Text Span
        if (item.type === "span") {
            return React.createElement("span", { key: i }, item.text);
        }
        //BR
        if (item.type === "br") {
            return React.createElement("br", { key: i });
        }
        //Null
        return null;
    };
    const generateBlock = (block) => {
        if (block.length === 0) {
            return React.createElement("br", null);
        }
        return (block.map((item, i) => {
            return generateItem(item, i);
        }));
    };
    return (content.map((item, i) => {
        //Block
        if (item.type === "block" && item.content) {
            return React.createElement("div", { className: ClassList.block + " " + (item.isCurrentBlock ? ClassList.currentBlock : ""), key: i }, generateBlock(item.content));
        }
        //Inline
        return generateItem(item, i);
    }));
};
/**
 * Parse html from div inner html to json[]
 * @param div
 */
exports.parseEditableDivContent = (wrapper) => {
    /**
     * Parse an html element to data
     * @param item
     */
    const parseNode = (node) => {
        //Image:
        let image;
        /**
         * Image
         */
        if (node.nodeName === "IMG") {
            image = node;
            return {
                type: "image",
                src: image.getAttribute("src") || undefined,
                size: image.classList.contains(ClassList.imgSmall) ? "small" : "normal"
            };
        }
        //TEXT
        if (node.textContent) {
            /**
                     * Text
                     */
            if (node.nodeName === "#text") {
                return {
                    type: "text",
                    text: node.textContent
                };
            }
            /**
             * Bold text
             */
            if (node.nodeName === "B") {
                return {
                    type: "b",
                    text: node.textContent
                };
            }
            /**
             * Italic text
             */
            if (node.nodeName === "I") {
                return {
                    type: "i",
                    text: node.textContent
                };
            }
            /**
             * Span
             */
            if (node.nodeName === "SPAN") {
                return {
                    type: "span",
                    text: node.textContent
                };
            }
            //Not valid:
            return {
                type: "text",
                text: node.textContent
            };
        }
        //BR
        if (node.nodeName === "BR") {
            return {
                type: "br",
            };
        }
        //Null
        return null;
    };
    /**
     * Parse a block content
     * @param block
     */
    const parseBlock = (block) => {
        //Content
        let content = [];
        //Children
        const nodes = block.childNodes;
        let itemData;
        for (let i = 0; i < nodes.length; i++) {
            itemData = parseNode(nodes[i]);
            if (itemData) {
                content.push(itemData);
            }
        }
        //Return
        return content;
    };
    //Content
    let content = [];
    //Children
    const nodes = wrapper.childNodes;
    let node, itemData, divBlock;
    let currentBlockIndex = 0, isCurrentBlock = false;
    for (let i = 0; i < nodes.length; i++) {
        node = nodes[i];
        /**
         * Div
         */
        if (node.nodeName === "DIV") {
            divBlock = node;
            isCurrentBlock = divBlock.classList.contains(ClassList.currentBlock);
            content.push({
                type: "block",
                content: parseBlock(divBlock),
                isCurrentBlock
            });
            //currentBlockIndex
            if (isCurrentBlock) {
                currentBlockIndex = i;
            }
            //NEXT
            continue;
        }
        itemData = parseNode(node);
        if (itemData) {
            content.push({
                type: "block",
                content: [itemData]
            });
        }
    }
    //Return
    return { content, currentBlockIndex };
};
/**
 * Validate url of an image
 */
const isImgUrl = (url, httpsOnly = true) => {
    let regEx = /(http(s?):)([/|.|\w|\s|\*|-])*\.(?:jpg|gif|png)/g;
    if (httpsOnly) {
        regEx = /(https:)([/|.|\w|\s|\*|-])*\.(?:jpg|gif|png)/g;
    }
    if (url.match(regEx) === null) {
        /**
         * Base 64
         */
        const base64Prefix = {
            jpg: "data:image/jpeg;base64,",
            png: "data:image/png;base64,",
        };
        //OK
        return url.startsWith(base64Prefix.png) || url.startsWith(base64Prefix.png);
    }
    //OK
    return true;
};
/**
 * Editable div
 */
class EditableDiv extends React.Component {
    /**
     * State
     */
    constructor(props) {
        super(props);
        //Virtual content
        this.virtualContent = null;
        //View content
        this.content = null;
        this.height = 0;
        /**
         * Sync content
         */
        this.syncContent = () => {
            var _a, _b;
            //Pass data
            if (this.content && ((_a = this.virtualContent) === null || _a === void 0 ? void 0 : _a.innerHTML)) {
                this.content.innerHTML = (_b = this.virtualContent) === null || _b === void 0 ? void 0 : _b.innerHTML;
            }
        };
        /**
         * Update content
         */
        this.updateContent = () => {
            if (!this.content) {
                return;
            }
            const { content, currentBlockIndex } = exports.parseEditableDivContent(this.content);
            this.setState({
                content,
                currentBlockIndex
            });
            //Trigger event
            const { onChange } = this.props;
            if (onChange) {
                onChange({ content, currentBlockIndex });
            }
        };
        /**
         * Handle paste
         */
        this.handlePaste = (e) => {
            //Selection
            const selection = document.getSelection();
            if (!selection) {
                return;
            }
            //Range
            const range = selection.getRangeAt(0);
            //Check clipboard
            const clipboardData = e.clipboardData;
            const text = clipboardData.getData("url") || clipboardData.getData("text");
            const pasteText = () => {
                //Insert image to div
                const div = document.createElement("div");
                div.innerHTML = text;
                range.insertNode(div);
                // Update new content
                this.updateContent();
            };
            if (isImgUrl(text)) {
                //Load image ok --> Prevent Paste
                e.preventDefault();
                //Load image
                const image = new Image();
                image.src = text;
                //Remove selection
                selection.empty();
                //Load event
                image.onload = () => {
                    //Insert image to div
                    range.insertNode(image);
                    //Update new content
                    this.updateContent();
                };
                image.onerror = () => {
                    pasteText();
                };
                //Stop
                return;
            }
            //Update new content
            setTimeout(() => {
                this.updateContent();
            }, 100);
        };
        /**
         * Blur --> Update
         */
        this.handleBlurContent = () => {
            if (!this.content) {
                return;
            }
            //Check caret
            this.checkCaretChange();
            //Content change
            if (this.previousContent == this.content.innerHTML) {
                return;
            }
            this.updateContent();
        };
        /**
         * Caret change
         */
        this.checkCaretChange = () => {
            //Save range
            const selection = document.getSelection();
            const range = selection && !selection.empty && selection.getRangeAt(0);
            let startNode = range && range.startContainer;
            while (startNode && startNode.nodeName !== "DIV") {
                startNode = startNode.parentNode;
            }
            if (!this.content) {
                return;
            }
            const blocks = this.content.getElementsByClassName(ClassList.block);
            for (let i = 0; i < blocks.length; i++) {
                blocks[i].classList.remove(ClassList.currentBlock);
            }
            const currentBlock = startNode;
            currentBlock && currentBlock.classList.add(ClassList.currentBlock);
            const { onChangeCaret } = this.props;
            if (onChangeCaret) {
                onChangeCaret();
            }
        };
        this.previousContent = "";
        this.handleFocusContent = () => {
            if (!this.content) {
                return;
            }
            this.previousContent = this.content.innerHTML;
        };
        /**
         * handleKeyDown
         */
        this.handleKeyDown = (e) => {
            if (!this.content) {
                return;
            }
            const { willDeleteMe } = this.state;
            const BACKSPACE_KEYCODE = 8;
            if (e.keyCode === BACKSPACE_KEYCODE) {
                if (!this.content.innerHTML) {
                    if (willDeleteMe) {
                        const { onDeleteMe } = this.props;
                        //onDeleteMe
                        if (onDeleteMe) {
                            onDeleteMe();
                        }
                    }
                    else {
                        this.setState({
                            willDeleteMe: true,
                            content: []
                        });
                    }
                    //Stop
                    return;
                }
            }
            if (willDeleteMe) {
                this.setState({
                    willDeleteMe: false
                });
            }
            // if (this.height !== this.content.clientHeight){
            //     this.updateContent()
            // }
        };
        this.handleDoubleClickContent = (e) => {
            const target = e.target;
            if (target && target.nodeName === "IMG") {
                target.classList.toggle(ClassList.imgSmall);
            }
        };
        /**
         * State:
         */
        this.state = {
            content: this.props.content,
            willDeleteMe: false,
            currentBlockIndex: -1
        };
    }
    /**
     * Did mount
     * 1. sync content from virtual to view
     */
    componentDidMount() {
        var _a;
        this.syncContent();
        //Height
        if ((_a = this.content) === null || _a === void 0 ? void 0 : _a.clientHeight) {
            this.height = this.content.clientHeight;
        }
    }
    componentDidUpdate() {
        var _a, _b;
        this.syncContent();
        //Height
        if ((_a = this.content) === null || _a === void 0 ? void 0 : _a.clientHeight) {
            this.height = (_b = this.content) === null || _b === void 0 ? void 0 : _b.clientHeight;
        }
    }
    //Render
    render() {
        const { className, placeholder } = this.props;
        const { content, willDeleteMe } = this.state;
        const style = {
            background: "white",
            border: willDeleteMe ? "1px solid red" : "initial"
        };
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { ref: c => this.content = c, style: style, placeholder: placeholder, contentEditable: true, suppressContentEditableWarning: true, onPaste: this.handlePaste, onBlur: this.handleBlurContent, onFocus: this.handleFocusContent, onKeyDown: this.handleKeyDown, onDoubleClick: this.handleDoubleClickContent, className: "ed-content " + (className !== null && className !== void 0 ? className : "") }),
            React.createElement("div", { ref: v => this.virtualContent = v, style: { visibility: "hidden", position: "absolute", left: "-9999999px", top: "-9999999px" } }, exports.generateEContent(content))));
    }
}
//Export
exports.default = EditableDiv;
