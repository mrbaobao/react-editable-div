"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ReactEditableDiv_1 = __importDefault(require("./ReactEditableDiv"));
//Export
exports.default = ReactEditableDiv_1.default;
