# react-editable-content-div

React editor using array content data

# Install:
* yarn add react-editable-content-div
* npm i react-editable-content-div

# Usage:
```
<ReactableDiv content={[]} className="" onChange={onChange}/>
```